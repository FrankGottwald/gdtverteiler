'''    
    GDTVerteiler
    Copyright (C) 2020  Frank Gottwald, Aralienweg 4, 64653 Lorsch
    f.gottwald@gottwald-it.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
from pathlib import Path

def start():
    strLicense= '''    
    *************************************************************************************************
    Copyright (C) 2020  Frank Gottwald, Aralienweg 4, 64653 Lorsch
    Email f.gottwald@gottwald-it.eu
    
    This program comes with ABSOLUTELY NO WARRANTY;  for details see LICENSE File in the app folder!
    This is free software, and you are welcome to redistribute it
    under certain conditions; for details see LICENSE File in the app folder!
    *************************************************************************************************
    '''
  
    ÜberwachungsOrdner="/Users/fg/Documents/repros/gdtverteiler/Beispiel"
    ZielordnerGDT="/Users/fg/Documents/repros/gdtverteiler/Beipiel/GDT"
    ZielOrdnerPDF="/Users/fg/Documents/repros/gdtverteiler/Beipiel/PDF"

    print('Überwachter Ordner:' + ÜberwachungsOrdner)
    print('Zielordner Ordner GDT:' + ZielordnerGDT)
    print('Zielordner Ordner PDF:' + ZielOrdnerPDF)

    # Prüfe Überwachungsordner
    if(not os.path.exists(ÜberwachungsOrdner)):
        print("Überwachungsordner existiert nicht ! (")
        input("Programm wird beendet. Bitte taste drücken")
        exit(0)
    # Prüfe Zielordner GDT
    if(not os.path.exists(ZielordnerGDT)):
        print("Zielordner GDT existiert nicht ! (")
        input("Programm wird beendet. Bitte taste drücken")
        exit(0)
        
    #Prüfe Zielordner PDF
    if(not os.path.exists(ZielOrdnerPDF)):
        print("Zielordner PDF existiert nicht ! (")
        input("Programm wird beendet. Bitte taste drücken")
        exit(0)

    
    meinÜW = Path(ÜberwachungsOrdner)
 
    
    

    

    print(strLicense + '\r\n')


    print('Hier geht es jetzt los:')
    print('Aktuelles Verzeichnig:' + os.getcwd())
    print(os.path.join(os.getcwd(),'Beispiel'))

    dateien = os.listdir(os.path.join(os.getcwd(),'Beispiel'))
        
    for s in dateien:
        print(s + ' - ' + ('true' if os.path.isdir(s) else 'false'))
      
        if os.path.isfile(os.path.join(os.getcwd(),'Beispiel',s)):
            print('Mit dir kann ich was anfangen:' + s)
            f=open(s, mode='r', encoding='UTF-8')
            strFile=f.read()
            print(strFile)
            f.close()
        else:
            print("nix gut :" + s)
            
    input('Taste drücken zum beenden !')


if __name__=="__main__":
    start()
