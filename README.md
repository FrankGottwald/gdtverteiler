# GDTVerteiler

Überwacht einen Ordner mit GDT Nachrichten die einen Befund / Rückgabe enthalten.
Das Programm prüft ob es in der GDT Datei verknüpfte Dateien wie EKGs als PDF gibt, prüft ob diese existieren und 
gibt diese Dateien samt GDT Datei an den Empfänger.